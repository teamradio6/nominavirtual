﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NominaVirtual.DataBase;
using NominaVirtual.Models;
using Nancy;
using Nancy.ModelBinding;


namespace NominaVirtual.Modules
{
    public class AreasModule : NancyModule
    {
        public AreasModule()
            : base("Areas")
        {
            Get["/"] = parameters =>
            {
                List<Area> _areas = EmployeeDB.GetAllAreas();

                return _areas;
            };
        }
    }
}
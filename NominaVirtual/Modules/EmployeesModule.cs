﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NominaVirtual.DataBase;
using NominaVirtual.Models;
using Nancy;
using Nancy.ModelBinding;


namespace NominaVirtual.Modules
{
    public class EmployeesModule : NancyModule
    {
        public EmployeesModule()
            : base("Employees")
        {
            Get["/"] = parameters =>
            {
                List<Employee> _employees = EmployeeDB.GetAllEmployees();
                return _employees;
            };

            Post["/Add"] = parameters =>
            {
                var employee = this.Bind<Employee>();

                Employee CreateEmployee = EmployeeDB.CreateEmployee(employee);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Update"] = parameters =>
            {
                var employee = this.Bind<Employee>();

                var result = EmployeeDB.UpdateEmployee(employee);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Get["/Company"] = parameters =>
            {
                List<Company> _companies = EmployeeDB.GetAllCompanies();
                return _companies;
            };

            Get["/{Identification}"] = parameters =>
            {
                var employeeid = (int)parameters.Identification;

                Employee _employee = EmployeeDB.GetEmployeeById(employeeid);
                return _employee;
            };

            Get["/ScheduleId/{ScheduleId}"] = parameters =>
            {
                var scheduleId = (int)parameters.ScheduleId;

                List<Employee> _employees = EmployeeDB.GetEmployeesByScheduleID(scheduleId);
                return _employees;
            };
        }
    }
}
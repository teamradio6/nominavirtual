﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NominaVirtual.DataBase;
using NominaVirtual.Models;
using Nancy;
using Nancy.ModelBinding;


namespace NominaVirtual.Modules
{
    public class HorariosModule : NancyModule
    {
        public HorariosModule()
            : base("Horarios")
        {
            Get["/"] = parameters =>
            {
                return HorarioDB.GetAllHorarios();
            };

            Post["/Add"] = parameters =>
            {
                var horario = this.Bind<Horario>();

                HorarioDB.CreateHorario(horario);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Update"] = parameters =>
            {
                var horario = this.Bind<Horario>();

                HorarioDB.UpdateHorario(horario);
                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Delete["/Delete/{ScheduleId}"] = parameters =>
            {
                var scheduleId = (int)parameters.ScheduleId;
                HorarioDB.DeleteSchedule(scheduleId);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}
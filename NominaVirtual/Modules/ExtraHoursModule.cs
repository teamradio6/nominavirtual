﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NominaVirtual.DataBase;
using NominaVirtual.Models;
using Nancy;
using Nancy.ModelBinding;


namespace NominaVirtual.Modules
{
    public class ExtraHoursModule : NancyModule
    {
        public ExtraHoursModule()
            : base("HorasExtra")
        {
            Get["/"] = parameters =>
            {
                var loggedUser = 6; //TODO: implementar Id de sesión
                return ExtraHourDB.GetAllExtraHoursByEMployeeID(loggedUser);
            };

            Get["/DetaillFiltered/{employeeId}/{filter}"] = parameters =>
            {
                var employeeId = (int)parameters.employeeId;
                var filter = (string)parameters.filter;
                return ExtraHourDB.GetEmployeeExtraHours(employeeId, filter);
            };

            Get["/Detaill/{IDReporteHE}"] = parameters =>
            {
                var IDReporteHE = (int)parameters.IDReporteHE;
                return ExtraHourDB.GetExtraHourDetaillByIdReport(IDReporteHE);
            };

            Get["/MyEmployees"] = parameters =>
            {
                var IdAprobador = (int)4; //TODO: implementar Id de sesión
                return ExtraHourDB.GetEmployeesByApprover(IdAprobador);
            };
            
            Post["/Add"] = parameters =>
            {
               var horaExtra = this.Bind<ExtraHour>();

               horaExtra.IDEmpleado = 6; //TODO: implementar Id de sesión
               horaExtra.Estado = 'S'; //Por defecto las plantillas quedan "sin Aprobar"
               horaExtra.Fecha = DateTime.Today;

               ExtraHourDB.CreateExtraHour(horaExtra);
               return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/Detaill/Add"] = parameters =>
            {
               var horaExtraDetalle = this.Bind<ExtraHourDetaill>();

               ExtraHourDB.CreateExtraHourDetaill(horaExtraDetalle);
               return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using NominaVirtual.Models;
using NominaVirtual.Services;

namespace NominaVirtual.DataBase
{
    public class ExtraHourDB
    {
        public static void CreateExtraHour(ExtraHour extraHour)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");
            db.HorasExtra.Insert(extraHour);
        }

        public static void CreateExtraHourDetaill(ExtraHourDetaill extraHourDetaill)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");
            db.HoraExtraDetalle.Insert(extraHourDetaill);
        }

        public static List<ExtraHourDetaill> GetExtraHourDetaillByIdReport(int _idReport)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");
            List<ExtraHourDetaill> extraHoursDetaill = db.HoraExtraDetalle.All()
                .Where(db.HoraExtraDetalle.IDReporteHE == _idReport);

            return extraHoursDetaill;
        }

        public static List<dynamic> GetEmployeeExtraHours(int _employeeId, string _filter)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<dynamic> extraHoursDetaill = db.sp_GetExtraHoursByEmployee(_employeeId, _filter);
            return extraHoursDetaill;
        }

        public static List<dynamic> GetEmployeesByApprover(int idApprover)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<dynamic> _employees = db.sp_GetEmployeesByApproverId(idApprover);

            return _employees;
        }

        public static dynamic GetAllExtraHoursByEMployeeID(int employeeId)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            var extraHours = db.sp_GetExtraHoursByEmployee(employeeId);
            return extraHours;
        }

        public static void UpdateExtraHour(ExtraHour extraHour)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            db.HorasExtra.UpdateByIDReporteHE(extraHour);
        }

        public static void DeleteSchedule(int _idReporteHE)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            db.HorasExtra.Delete(IDReporteHE: _idReporteHE);
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using NominaVirtual.Models;

namespace NominaVirtual.DataBase
{
    public class AreaDB
    {
        public static dynamic CreateArea(Area area)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            var CreateArea = db.Empleados.Insert(area);
            return CreateArea;
        }
    }
}
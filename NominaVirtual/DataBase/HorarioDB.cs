﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using NominaVirtual.Models;
using NominaVirtual.Services;

namespace NominaVirtual.DataBase
{
    public class HorarioDB
    {
        public static void CreateHorario(Horario horario)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");
            HorarioNullable insertHorario = HorarioService.ConvertHorarioInNullable(horario);

            db.Horarios.Insert(insertHorario);
        }

        public static List<HorarioNullable> GetAllHorarios()
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<HorarioNullable> horarios = db.Horarios.All();
            return horarios;
        }        

        public static void UpdateHorario(Horario horario)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            HorarioNullable updateHorario = HorarioService.ConvertHorarioInNullable(horario);

            db.Horarios.UpdateByIDHorario(updateHorario);
        }

        public static void DeleteSchedule(int idHorario)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            db.Horarios.Delete(IDHorario: idHorario);
        }
    }
}
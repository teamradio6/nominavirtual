﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using NominaVirtual.Models;

namespace NominaVirtual.DataBase
{
    public class EmployeeDB
    {
        public static dynamic CreateEmployee(Employee employee)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            var CreateEmployee = db.Empleados.Insert(employee);
            return CreateEmployee;
        }

        public static dynamic CreateArea(Area area)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            var CreateArea = db.Empleados.Insert(area);
            return CreateArea;
        }

        public static List<Company> GetAllCompanies()
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<Company> _companies = db.Empresas.All();
            return _companies;
        }

        public static Employee GetEmployeeByIdentification(long identification)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            Employee client = db.Clientes.All()
                .Where(db.empleados.Cedula == identification).FirstOrDefault();

            return client;
        }

        public static List<Employee> GetAllEmployees()
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<Employee> _employees = db.Empleados.All();
            return _employees;
        }

        public static List<Area> GetAllAreas()
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<Area> _areas = db.Areas.All();
            return _areas;
        }

        public static Employee GetEmployeeById(int identification)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            Employee employee = db.Empleados.All()
                .Where(db.Empleados.Cedula == identification).FirstOrDefault();

            return employee;
        }


        public static dynamic UpdateEmployee(Employee employee)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            var updateEmployee = db.Empleados.UpdateByIDEmpleado(employee);
            return updateEmployee;
        }

        public static List<Employee> GetEmployeesByScheduleID( int scheduleID)
        {
            var db = Database.OpenNamedConnection("NominaVirtual");

            List<Employee> employees = db.Empleados.All()
                .Where(db.Empleados.IDHorario == scheduleID);

            return employees;
        }
    }
}
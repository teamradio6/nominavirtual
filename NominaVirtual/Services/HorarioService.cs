﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NominaVirtual.Models;
using System.Reflection;

namespace NominaVirtual.Services
{
    public class HorarioService
    {
        public static HorarioNullable ConvertHorarioInNullable(Horario horario){

            HorarioNullable newScheduler = new HorarioNullable();

            foreach (PropertyInfo propertyInfo in horario.GetType().GetProperties())
            {
                foreach (PropertyInfo propertyInfoInsert in newScheduler.GetType().GetProperties())
                {
                    if (propertyInfo.Name == propertyInfoInsert.Name)
                    {
                        //Id Horario
                        if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(int?))
                        {
                            int? propertyValue = (int?)propertyInfo.GetValue(horario);
                            if (propertyValue == null)
                            {
                                propertyInfoInsert.SetValue(newScheduler, null);
                            }
                            else
                            {
                                propertyInfoInsert.SetValue(newScheduler, propertyValue, null);
                            }
                        }
                        //Horas
                        else if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(TimeSpan))
                        {
                            Nullable<TimeSpan> propertyValue = (Nullable<TimeSpan>)propertyInfo.GetValue(horario);
                            if (!object.Equals(propertyValue, TimeSpan.Zero))
                            {
                                propertyInfoInsert.SetValue(newScheduler, propertyValue);
                            }
                            else
                            {
                                propertyInfoInsert.SetValue(newScheduler, null);
                            }
                        }
                        //Descripcion
                        else if (propertyInfo.CanRead && propertyInfo.PropertyType == typeof(string))
                        {
                            string propertyValue = (string)propertyInfo.GetValue(horario);
                            propertyInfoInsert.SetValue(newScheduler, propertyValue, null);
                        }
                    };

                };
            };

            return newScheduler;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NominaVirtual.Models
{
    public class ExtraHour
    {
        public int? IDReporteHE { get; set; }
        public int IDEmpleado { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public char Estado { get; set; }
    }

}
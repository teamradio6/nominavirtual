﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NominaVirtual.Models
{
    public class ExtraHourDetaill
    {
        public int? IDHoraExtraDetalle { get; set; }
        public int IDReporteHE { get; set; }        
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Observaciones { get; set; }
        public char? Estado { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NominaVirtual.Models
{
    public class Horario
    {
        public int? IDHorario{ get; set; }
        public string Descripcion { get; set; }
        public TimeSpan LunesEntrada { get; set; }
        public TimeSpan LunesSalida { get; set; }
        public TimeSpan MartesEntrada { get; set; }
        public TimeSpan MartesSalida { get; set; }
        public TimeSpan MiercolesEntrada { get; set; }
        public TimeSpan MiercolesSalida { get; set; }
        public TimeSpan JuevesEntrada { get; set; }
        public TimeSpan JuevesSalida { get; set; }
        public TimeSpan ViernesEntrada { get; set; }
        public TimeSpan ViernesSalida { get; set; }
        public TimeSpan SabadoEntrada { get; set; }
        public TimeSpan SabadoSalida { get; set; }
        public TimeSpan DomingoEntrada { get; set; }
        public TimeSpan DomingoSalida { get; set; }
    }

    public class HorarioNullable
    {
        public int? IDHorario { get; set; }
        public string Descripcion { get; set; }
        public Nullable<TimeSpan> LunesEntrada { get; set; }
        public Nullable<TimeSpan> LunesSalida { get; set; }
        public Nullable<TimeSpan> MartesEntrada { get; set; }
        public Nullable<TimeSpan> MartesSalida { get; set; }
        public Nullable<TimeSpan> MiercolesEntrada { get; set; }
        public Nullable<TimeSpan> MiercolesSalida { get; set; }
        public Nullable<TimeSpan> JuevesEntrada { get; set; }
        public Nullable<TimeSpan> JuevesSalida { get; set; }
        public Nullable<TimeSpan> ViernesEntrada { get; set; }
        public Nullable<TimeSpan> ViernesSalida { get; set; }
        public Nullable<TimeSpan> SabadoEntrada { get; set; }
        public Nullable<TimeSpan> SabadoSalida { get; set; }
        public Nullable<TimeSpan> DomingoEntrada { get; set; }
        public Nullable<TimeSpan> DomingoSalida { get; set; }
    }
}
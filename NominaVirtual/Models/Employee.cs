﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NominaVirtual.Models
{
    public class Employee
    {
        public int IDEmpleado { get; set; }
        public long Cedula { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Cargo { get; set; }
        public int IDArea { get; set; }
        public string Telefono { get; set; }
        public string Movil { get; set; }
        public string Email { get; set; }
        public Boolean Estado { get; set; }
        public int IDEmpresa { get; set; }
        public int IDHorario { get; set; }
        public Boolean Aprobador { get; set; }
        public char Genero { get; set; }
    }
}
﻿payRollApp.controller('createEmployee_Ctrl', ['$scope', 'Areas', 'Employees', 'Horarios', 'Notifications', function ($scope, Areas, Employees, Horarios, Notifications) {

    $scope.employee = {};

    $scope.Limpiar = function () {
        $scope.employee = {};
    }

    $scope.Save = function () {
        if ($scope.employee.iDEmpleado !== undefined) {
            Employees.update.UpdateEmployee({}, $scope.employee);
        }
        else {
            Employees.add.AddEmployee({}, $scope.employee);
        }
        $scope.Limpiar();
    };

    $scope.Validate = function (_identification) {
        Employees.validate.ValidateEmployee({ identification: _identification }).$promise.then(function (_employee) {
            if (_employee.iDEmpleado !== undefined) {
                $scope.employee = _employee;
            }
            else {
                Notifications.info.EmployeeDoesNotExist();
                $scope.employee = {};
                $scope.employee.cedula = _identification;
            }
        });
    };
    
    $scope.areas = Areas.Area.GetAreas();
    $scope.factories = Employees.company.GetCompanies();
    $scope.horarios = Horarios.Get.GetHorarios();
    $scope.empleados = Employees.employee.GetEmployees();

    /* DATEPICKER  (Fecha Ingreso)*/
    $scope.hoy = function () {
        $scope.employee.fechaIngreso = new Date();
    };

    $scope.limpiarFecha = function () {
        $scope.employee.fechaIngreso = null;
    };

    $scope.limpiarFecha();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.format = 'dd/MM/yyyy';
    /**/

}])
payRollApp.controller('detaillHours_Ctrl', ['$scope', '$stateParams', 'HorasExtra', function ($scope, $stateParams, HorasExtra) {

    $scope.upstertDetaill = {
        observaciones: '',
        fechaEntrada: new Date(),
        fechaSalida: new Date()
    };
    $scope.extraHourDetaills = [];

    $('#inDate').datetimepicker({
        defaultDate: new Date(),
        format: 'DD/MM/YYYY h:mm a'
    });
    $('#outDate').datetimepicker({
        defaultDate: new Date(),
        format: 'DD/MM/YYYY h:mm a'
    });

    $("#inDate").on("dp.change", function (e) {
        $scope.upstertDetaill.fechaEntrada = e.date;
        $scope.$apply();
    });

    $("#outDate").on("dp.change", function (e) {
        $scope.upstertDetaill.fechaSalida = e.date;
        $scope.$apply();
    });

    $scope.GetExtraHours = function () {
        $scope.extraHoursApprover = HorasExtra.GetFiltered.GetHorasExtra({ employeeId: $stateParams.employeeId, filter: $stateParams.filter });
        console.log($scope.extraHoursApprover);
    };

    $scope.GetExtraHoursDetaill = function (_reportId) {
        $scope.extraHourDetaills = [];
        $scope.extraHourDetaills = HorasExtra.GetDetaill.GetHorasExtraDetalle({ IDReporteHE: _reportId });
        $scope.upstertDetaill.iDReporteHE = _reportId;
    };

    $scope.AddDetail = function () {

       alert(' En Construcción');
    };

    $scope.GetExtraHours();

}])
payRollApp.controller('addHours_Ctrl', ['$scope', 'HorasExtra', function ($scope, HorasExtra) {

    $scope.upstertDetaill = {
        observaciones: '',
        fechas: null,
        horaInicial: new Date(),
        horaFinal: new Date()
    };
    $scope.extraHourDetaills = [];

    $('#inDate').datepicker({
        format: 'dd/mm/yyyy',
        orientation: 'bottom auto',
        multidate: true
    });

    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.ismeridian = true;

    $scope.GetExtraHours = function () {
        $scope.extraHours = HorasExtra.Get.GetHorasExtra();
    };

    $scope.GetExtraHoursDetaill = function (_reportId) {
        $scope.extraHourDetaills = [];
        $scope.extraHourDetaills = HorasExtra.GetDetaill.GetHorasExtraDetalle({ IDReporteHE: _reportId });
        $scope.upstertDetaill.iDReporteHE = _reportId;
    };

    $scope.GetExtraHours();
    

    $scope.AddTemplate = function () {
        HorasExtra.Create.AddExtraHour({}, $scope.extraHourAdd).$promise.then(function () {
            $scope.GetExtraHours();
        });
    };

    $scope.AddDetail = function () {
        if ($scope.upstertDetaill.IDHoraExtraDetalle == null) {
            $scope.upstertDetaill.estado = null;
            $scope.upstertDetaill.fechas = $scope.upstertDetaill.fechas.split(',');

            
            HorasExtra.AddDetaill.AddExtraHourDetaill({}, $scope.upstertDetaill).$promise.then(function () {
                $scope.GetExtraHoursDetaill($scope.upstertDetaill.iDReporteHE);
                $scope.GetExtraHours();
                $scope.upstertDetaill = {};
            });
        }
    };

}])
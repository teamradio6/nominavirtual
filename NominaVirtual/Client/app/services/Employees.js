﻿payRollApp.factory('Employees', ['$resource', function ($resource) {

    return {
        employee:
            $resource('Employees', {},
                {
                    "GetEmployees": { method: "GET", params: {}, isArray: true }
                }
            ),
       	add:
       		$resource('Employees/Add', {},
	       		{
	       			"AddEmployee": { method : "POST", params: {}, isArray: false }
	       		}
	       	),
       	update:
       		$resource('Employees/Update', {},
	       		{
	       		    "UpdateEmployee": { method: "PUT", params: {}, isArray: false }
	       		}
	       	),
       	validate:
       		$resource('Employees/:identification', { identification: '@identification' },
	       		{
	       		    "ValidateEmployee": { method: "GET", params: { identification: '@identification' }, isArray: false }
	       		}
	       	),
	  	company:
       		$resource('Employees/Company', {},
	       		{
	       		    "GetCompanies": { method: "GET", params: {}, isArray: true, ignoreLoadingBar: true }
	       		}
	       	),
	  	employeesByScheduleID:
       		$resource('Employees/ScheduleId/:ScheduleId', { ScheduleId: '@ScheduleId' },
	       		{
	       		    "GetEmployees": { method: "GET", params: { ScheduleId: '@ScheduleId' }, isArray: true }
	       		}
	       	)
    };

}]);
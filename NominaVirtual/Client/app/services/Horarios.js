payRollApp.factory('Horarios', ['$resource', function ($resource) {

    return {
        Get:
            $resource('Horarios', {},
                {
                    "GetHorarios": { method: "GET", params: {}, isArray: true, ignoreLoadingBar: true }
                }
            ),

       	Create:
       		$resource('Horarios/Add', {},
	       		{
	       			"AddHorario": { method : "POST", params: {}, isArray: false }
	       		}
	       	),
       	Update:
            $resource('Horarios/Update', {},
	       		{
	       		    "UpdateHorario": { method : "PUT", params: {}, isArray: false }
	       		}
	       	),
       	Delete:
            $resource('Horarios/Delete/:ScheduleId', { ScheduleId: '@ScheduleId' },
	       		{
	       		    "DeleteSchedule": { method: "Delete", params: { ScheduleId: '@ScheduleId' }, isArray: false }
	       		}
	       	)
    };

}]);
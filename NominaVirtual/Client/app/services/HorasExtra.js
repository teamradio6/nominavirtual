payRollApp.factory('HorasExtra', ['$resource', function ($resource) {

    return {
        Get:
            // $resource('HorasExtra', {},
            $resource('Client/app/jsons/hour.json', {},
                {
                    "GetHorasExtra": { method: "GET", params: {}, isArray: true }
                }
            ),
        GetFiltered:
            $resource('HorasExtra/DetaillFiltered/:employeeId/:filter', { employeeId: '@employeeId', filter: '@filter' },
                {
                    "GetHorasExtra": { method: "GET", params: { employeeId: '@employeeId', filter: '@filter' }, isArray: true }
                }            
            ),
        GetDetaill:
            // $resource('HorasExtra/Detaill/:IDReporteHE', { IDReporteHE: '@IDReporteHE' },
                $resource('Client/app/jsons/DETAILHOUR.json', { IDReporteHE: '@IDReporteHE' },                
                {
                    "GetHorasExtraDetalle": { method: "GET", params: { IDReporteHE: '@IDReporteHE' }, isArray: true }
                }
            ),
        GetEmployees:
            $resource('HorasExtra/MyEmployees', {},
                {
                    "GetByApproverId": { method: "GET", params: { }, isArray: true }
                }
            ),
       	Create:
       		$resource('HorasExtra/Add', {},
	       		{
	       		    "AddExtraHour": { method: "POST", params: {}, isArray: false }
	       		}
	       	),
       	AddDetaill:
       		$resource('HorasExtra/Detaill/Add', {},
	       		{
	       		    "AddExtraHourDetaill": { method: "POST", params: {}, isArray: false }
	       		}
	       	),
       	Update:
            $resource('HorasExtra/Update', {},
	       		{
	       		    "UpdateExtraHour": { method : "PUT", params: {}, isArray: false }
	       		}
	       	),
       	//Delete:
        //    $resource('HorasExtra/Delete/:ScheduleId', { ScheduleId: '@ScheduleId' },
	    //   		{
	    //   		    "DeleteSchedule": { method: "Delete", params: { ScheduleId: '@ScheduleId' }, isArray: false }
	    //   		}
	    //   	)
    };

}]);
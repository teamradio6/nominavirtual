payRollApp.factory('Areas', ['$resource', function ($resource) {

    return {
        Area:
            $resource('Areas', {},
                {
                    "GetAreas": { method: "GET", params: {}, isArray: true, ignoreLoadingBar: true }
                }
            ),

       	add:
       		$resource('Areas/Add', {},
	       		{
	       			"AddArea": { method : "POST", params: {}, isArray: false }
	       		}
	       	)
    };

}]);
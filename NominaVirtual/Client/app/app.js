var payRollApp = angular.module('payRollApp', ['ngResource', 'ui.router', 'ui.bootstrap', 'ngRoute', 'ngGrid', 'angular-loading-bar'])

    .value("toastr", toastr)

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

        // Informe RAG
        .state('/', {
            url: '/',
            controller: 'homeCtrl',
            templateUrl: 'Client/app/templates/home.html'
        })

        //Employees

            //Create
            .state('/createEmployee', {
                url: '/createEmployee',
                controller: 'createEmployee_Ctrl',
                templateUrl: 'Client/app/templates/employees/createEmployee.html'
            })

            //Search
            .state('/searchEmployee', {
                url: '/searchEmployee',
                controller: 'searchEmployee_Ctrl',
                templateUrl: 'Client/app/templates/employees/searchEmployee.html'
            })

             //Manage Schedules
            .state('/schedules', {
                url: '/schedules',
                controller: 'manageSchedules_Ctrl',
                templateUrl: 'Client/app/templates/employees/manageSchedules.html'
            })


        //payRolls

            //Add
            .state('/addHours', {
                url: '/addHours',
                controller: 'addHours_Ctrl',
                templateUrl: 'Client/app/templates/extraHours/addHours.html'
            })

            //Approve
            .state('/approveHours', {
                url: '/approveHours',
                controller: 'approveHours_Ctrl',
                templateUrl: 'Client/app/templates/extraHours/approveHours.html'
            })

            //Detaill
            .state('/detaill', {
                url: '/detaill/:employeeId/:filter',
                controller: 'detaillHours_Ctrl',
                templateUrl: 'Client/app/templates/extraHours/detaillHours.html'
            })


        $httpProvider.interceptors.push('Interceptor');

            
    }])